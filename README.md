# ssRapid

[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](http://makeapullrequest.com)

ssRapid is a fast implementation of the popular Shadowsocks proxy used by companies such as [Alphabet](https://www.wired.com/story/alphabet-outline-vpn-software/) for installation on a VPS, Raspberry Pi, or that old computer you have sitting in your closet.

The problem with similar implementations, such as [Outline](https://getoutline.org/) or [Streisand](https://github.com/StreisandEffect/streisand) is that they are often closed-source, poorly documented and also usually take up the entire server, not allowing easy installation of other utilities on the same machine, such as a web server.

ssRapid is based on `shadowsocks-libev` port of shadowsocks maintained by madeye [here](https://github.com/shadowsocks/shadowsocks-libev), and installs through a bash shell script. You can read more about the shadowsocks protocol [here](https://shadowsocks.org/en/index.html).

**The guiding aims are:**
- Performant and efficient.
- Simple to set up, powerful when necessary.
- Secure by default.
- Free and open source, playing nicely with anything else you want on your machine.

**Quick links:**

Install and Use | Troubleshooting | Contributing | Support us!
--- | --- | --- | ---
[Installation guide](https://gitlab.com/Clae/ssrapid#installation), [usage guide](https://gitlab.com/Clae/ssrapid#usage). Example configuration files can be found [here](https://gitlab.com/Clae/ssrapid/tree/master/configuration-examples). | [Troubleshooting](https://gitlab.com/Clae/ssrapid#troubleshooting). [Installation problems](https://gitlab.com/Clae/ssrapid#installation-issues), [usage problems](https://gitlab.com/Clae/ssrapid#usage-issues). | [Contributing](https://gitlab.com/Clae/ssrapid/blob/master/CONTRIBUTING.md). | [Donate](https://liberapay.com/Clae/donate) if you love us! Stars, forks and sharing this project helps a lot too!

##### Roadmap

- [ ] Randomly generate secure passwords by default
- [ ] Complete todos noted in setupScript.sh
- [x] Add support for more distros
- [x] Add option to build from source
- [ ] Add more error checks for installer script
- [x] Fork and mirror repos into this repo for easy building from scratch

## Installation

### Prerequisites
- Linux machine with `bash`

*Note that it is possible to run this on other platforms with bash, but they are not supported as network configuration will vary wildly.*

### Installation steps

**On the machine you want to host the VPN/proxy:**

1. Clone or download this repository.
    - Clone by typing `git clone https://gitlab.com/Clae/ssrapid.git` into a terminal.
    - Download through [this link](https://gitlab.com/Clae/ssrapid/-/archive/master/ssrapid-master.zip). Extract into a folder.

    *You can download to `/tmp/` if you do not want to keep installer files after installation.*


2. Navigate into the repository folder in a terminal.
   - If you do not know how to use `cd` to do this, go into the ssRapid folder using your file explorer, right click on an empty spot and there should be an option to "Open terminal here".
   - If you used `git clone`, the repository folder is probably at `~/ssRapid` so you can go there by typing `cd ~/ssRapid`.


3. Run the ssRapid installer by typing `./setupScript.sh` in the terminal.
   - If you get `No such file or directory` error, you are not in the correct folder and didn't complete step 2 correctly.
   - If you get an error to do with permissions, you will need to type `chmod 700 setupScript.sh` first, then type `./setupScript.sh` again.


4. Follow the instructions in the terminal. If you're not sure about something, just press enter without typing anything for default settings.

Once the script finishes, you have successfully installed a shadowsocks server. You now just need a [client app](https://shadowsocks.org/en/download/clients.html) on any device you would lke to connect, and to launch the server (see below).

## Usage

Typing `ss-server -c ~/shadowsocks/shadowsocks-config.json` will launch the server. You should be able to connect using a [correctly configured client](https://gitlab.com/Clae/ssrapid#configuring-the-client) after launching the server.

By default, the VPN/proxy service will close when you close the terminal you launched it from. If you would like to close the terminal but keep the service running, you can run it in the background by pressing `Ctrl+Z` while the service is running, then typing `bg` into the terminal, which will run it in the background. To inspect the service again, you can type `fg` to being it back into the foreground. Alternatively, you can use `tmux`.

You can run the service in verbose mode for more information with the `-v` tag, i.e. launch the service by typing `ss-server -v -c ~/shadowsocks/shadowsocks-config.json`.

For more information about encryption and cipher suites, see [here](https://shadowsocks.org/en/spec/Stream-Ciphers.html). It is recommended that you use a [AEAD cipher](https://shadowsocks.org/en/spec/AEAD-Ciphers.html) when possible. *Not all supported ciphers are guaranteed to be secure!*

##### Reconfiguration

You may find that you need to reconfigure the server. Simply re-run `./setupScript.sh` by re-doing step 3 and 4 of the installation steps. Note that this **will override** the previous configuration. You can back up previous configurations by renaming them, such as `cp ~/shadowsocks/shadowsocks-config.json ~/shadowsocks/shadowsocks-config-old.json`. This will allow you to launch shadowsocks with older configuration files.

You should not need to reinstall any packages when reconfiguring.

### Configuring the Client

To configure the client, use the settings you used when configuring the server such as port number, password, cipher, etc. These were reported back to you when you set it up. Alternatively, you can look at the configuration file anytime at `~/shadowsocks/shadowsocks-config.json` with `nano ~/shadowsocks/shadowsocks-config.json` or another text editor you prefer. Make sure all available configuration options on the client matches with the server.

If you are asked for a QR code, select "Use manual configuration" or a similar option as there is currently no way to generate QR codes securely.

If you are asked to "add an access key", you have probably installed the Outline VPN server. Install another app that supports the shadowsocks protocol completely.

## Troubleshooting

### Installation issues

**`Cannot install packages automatically`:** The installer script can't find the package manager automatically, encountered an error whilst installing, or installed broken packages. You should try to manually install `shadowsocks-libev` and dependencies by following the [maintainer's instructions](https://github.com/shadowsocks/shadowsocks-libev#installation).

**`Package error`:** This is a general error indicating that something is not installed correctly. See above. You may need to manually install the `simple-obfs` package if you would like to use obfuscation.

**`configure: error: Cannot find '[package]' in PATH.`:** A prerequisite for building/compiling is not met. Install the missing package manually. If it persists, see documentation [here](https://github.com/shadowsocks/shadowsocks-libev#linux).

**`configure: error: The '[library]' library is missing.`:** A prerequisite for building/compiling is not met. Install the missing library manually. If it persists, see documentation [here](https://github.com/shadowsocks/shadowsocks-libev#linux).

### Usage issues

#### General networking and configuration issues:

- Check that every available configuration option matches - make sure the port number, password, cipher option, etc. all match between the server and client. If some are unavailable on the client<sup>1</sup>, you may need to [reconfigure](https://gitlab.com/Clae/ssrapid#reconfiguration) the server to one that matches.
- Check that there are no other connection related blocking issues. Often, ports other than 80 and 443 are blocked/firewalled. You may need to [reconfigure](https://gitlab.com/Clae/ssrapid#reconfiguration) shadowsocks to use these ports.
- VPS services may also have default port blocking policies for security<sup>2</sup>, or your ports may not be forwarded correctly. Make sure the correct security policies are applied in your VPS management console, or in your home router, depending on where your server machine is located.

**Client devices have no internet connection but report they are connected:** Some background first - the shadowsocks protocol is unlike many other VPN protocols in that it does not need to establish a handshake prior to data transmission. This means that most client apps report "connected" even when there are major issues. This issue may be any problem with configuration, networking, or a combination of both. To fix, see the general usage issue troubleshooting list above. You may need to manually install the `simple-obfs` package if you would like to use obfuscation.

**VPN/proxy server dies when the terminal window is closed:** This is default behaviour of shadowsocks-libev. See the [Usage](https://gitlab.com/Clae/ssrapid#usage) section about overcoming this.

**Issues with `fg` and `bg`:** There may be other suspended or background terminal services. Type `jobs -l` to see all the jobs. You may need to run the job number, such as `bg 3` if ss-server is job 3.

*<sup>1</sup> An example is that some client apps, such as iOS shadowsocks apps, don't support the default `chacha20-ietf-poly1305` cipher. If this is the case, and you have configured the server with this cipher, you need to reconfigure with another cipher such as `aes-256-gcm`.*

*<sup>2</sup> Amazon web services, as an example, will do this by default. For AWS you will need to add a "security group" insluding the inbound port you set in the installation step, and outbound ports for HTTP and HTTPS/TLS traffic (80 and 443 respectively).* 