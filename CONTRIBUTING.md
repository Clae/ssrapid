## Contributing

[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](http://makeapullrequest.com)

When contributing to this project, please first discuss changes through Issues or through email. Pull requests are welcome too, but prior discussion is preferred.

It is expected that changes are well documented in the Pull Request, along with relevant changes to README.md and comments in the code.