#! /bin/bash

exists () {
    type "$1" >/dev/null 2>/dev/null
}

if ! exists "bash"; then
    echo "You need to install the bash package"
    exit 1
fi

set -e

if ! exists "ss-server"; then

    read -p "Would you like to automatically install shadowsocks-libev and prerequisites? Y/n: " -n 1 -r
    if [[ $REPLY =~ ^[Yy]$ ]]
    then

        echo
        echo
        echo Auto-installing shadowsocks-libev and prerequisites...

        if exists "apt-get"; then
            PKG_APP="apt-get -y install"
        elif exists "dnf"; then
            PKG_APP="dnf -y install"
        elif exists "yum"; then
            PKG_APP="yum -y install"
        elif exists "emerge"; then
            PKG_APP="emerge"
        elif exists "zypper"; then
            PKG_APP="zypper in"
        elif exists "pkg"; then
            PKG_APP="pkg install -y"
        else
            echo "Cannot install packages automatically. Attempting to build and install."
            PKG_APP="MANUAL_BUILD"
        fi
    else
        echo
        echo
        read -p "Would you like to build shadowsocks-libev and prerequisites? Y/n: " -n 1 -r
        if [[ $REPLY =~ ^[Yy]$ ]]
        then
            PKG_APP="MANUAL_BUILD"
        else
            exit 2
        fi
    fi

    if [ "$PKG_APP" != "MANUAL_BUILD" ] 
    then
        { # try

            eval "$PKG_APP shadowsocks-libev simple-obfs"

        } || { # catch

            eval "sudo $PKG_APP shadowsocks-libev simple-obfs"

        }
    else
        if ! exists "git"; then
            echo "Cannot build and install - please install git package."
            exit 3
        else
            { # try

                echo
                cd shadowsocks-libev/ &&
                cd ..

            } || { # except

                git clone https://github.com/shadowsocks/shadowsocks-libev.git
                cd shadowsocks-libev
                git submodule update --init --recursive

            }

            export LIBSODIUM_VER=1.0.16
            wget https://download.libsodium.org/libsodium/releases/libsodium-$LIBSODIUM_VER.tar.gz
            tar xvf libsodium-$LIBSODIUM_VER.tar.gz
            pushd libsodium-$LIBSODIUM_VER
            ./configure --prefix=/usr && make
            sudo make install
            popd
            sudo ldconfig
            
            export MBEDTLS_VER=2.6.0
            wget https://tls.mbed.org/download/mbedtls-$MBEDTLS_VER-gpl.tgz
            tar xvf mbedtls-$MBEDTLS_VER-gpl.tgz
            pushd mbedtls-$MBEDTLS_VER
            make SHARED=1 CFLAGS="-O2 -fPIC"
            sudo make DESTDIR=/usr install
            popd
            sudo ldconfig
            
            cd shadowsocks-libev/

            ./autogen.sh && ./configure && make
            sudo make install

            cd ..
        fi
    fi
fi

if ! exists "ss-server"; then
    echo "Package error"
    exit 4
fi

while true; do
    echo
    read -p "Please enter a desired open port (or press enter for a random port between 1000-65535): " port

    if [ $port = "" ]
    then
        port=$(shuf -i1000-65535 -n1) # random integer between 1000 and 65535
        echo "Port is $port"
        break
    elif [ $port -gt 0 ] && [ $port -lt 65536 ]
    then
        break
    else
        echo "Invalid port, please enter a valid port between 1 and 65535 inclusive."
    fi
done

while true; do
    echo
    read -sp "Please enter your password (this will NOT be echoed): " password

    #TODO: randomly generate strings
    if [ ${#password} -lt 6 ]
    then
        echo
        echo
        echo "Invalid password, please enter a valid password that is longer than 5 characters."
        continue
    fi

    echo
    read -sp "Enter your password again: " verify
    if [ $verify != $password ]
    then
        echo 
        echo
        echo "Passwords did not match! Try again."
        continue
    fi

    break
done

validMethods=(aes-128-gcm aes-192-gcm aes-256-gcm rc4-md5 aes-128-cfb aes-192-cfb aes-256-cfb aes-128-ctr aes-192-ctr aes-256-ctr bf-cfb camellia-128-cfb camellia-192-cfb camellia-256-cfb chacha20-ietf-poly1305 salsa20 chacha20 and chacha20-ietf)

while true; do
    echo
    echo
    echo "Please enter an encryption method, using lowercase (or press enter for default chacha20-ietf-poly1305)."
    echo "You can read more about encryption methods available here: https://shadowsocks.org/en/spec/AEAD-Ciphers.html and here: https://shadowsocks.org/en/spec/Stream-Ciphers.html "
    read -p "Your chosen method: " method
    
    # Check if method is valid
    
    if [[ $method = "" ]]; then # use default
        chosenMethod="chacha20-ietf-poly1305"
        echo
        echo "Default method $chosenMethod selected."
        break
    elif [[ "${validMethods[*]}" =~ (^|[^[:alpha:]])$method([^[:alpha:]]|$) ]]
    then
        chosenMethod=$(echo "$method" | tr '[:upper:]' '[:lower:]')
        echo "Method $chosenMethod selected."
        break
    else
        echo
        echo "Please enter a valid method. Valid cipher methods are aes-128-gcm, aes-192-gcm, aes-256-gcm, rc4-md5, aes-128-cfb, aes-192-cfb, aes-256-cfb, aes-128-ctr, aes-192-ctr, aes-256-ctr, bf-cfb, camellia-128-cfb, camellia-192-cfb, camellia-256-cfb, chacha20-ietf-poly1305, salsa20, chacha20 and chacha20-ietf. "
        echo "You can read more about encryption methods available here: https://shadowsocks.org/en/spec/AEAD-Ciphers.html and here: https://shadowsocks.org/en/spec/Stream-Ciphers.html"
    fi
done

while true; do
    echo
    read -p "Enter a DNS server IPv4 (press enter for default 1.1.1.1): " dnsServer

    if [ $dnsServer="" ]
    then
        dnsServer="1.1.1.1"
        break
    else
        #TODO check if dns server is valid
        break
    fi
done

cd ~
mkdir shadowsocks
cd shadowsocks
touch shadowsocks-config.json
chmod 600 shadowsocks-config.json

read -p "Would you like to use obfuscation? Y/n: " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then

    # if ! exists "simple-obfs"; then
    #     echo "simple-obfs package seems to not be installed. This will likely result in a broken configuration!"
    #     read -p "Continue? Y/n: " -n 1 -r
    #     if [[REPLY =~ ^[Yy]$ ]] then
    #         echo
    #         echo
    #         echo "Continuing..."
    #     else
    #         echo
    #         echo
    #         echo "Please install simple-obfs manually."
    #         echo
    #         echo "Package error"
    #         exit 4
    #     fi
    # fi

    while true; do
        read -p "tls or https obfuscation? " obfsType

        if [ $obfsType="tls" ] || [ $obfsType="https" ]
        then
            break
        else
            #TODO change to lowercase in case of upper case.
            echo "Please enter tls or https, lowercase."
            continue
        fi
    done

    cat >./shadowsocks-config.json <<EOL
{
"server":["::0", "0.0.0.0"],
"server_port":"${port}",
"password":"${password}",
"method":"${chosenMethod}",
"timeout":15,
"fast_open":true,
"reuse_port":true,
"plugin":"obfs-server",
"plugin_opts":"obfs=${obfsType}",
"nameserver":"${dnsServer}",
"mode":"tcp_and_udp"
}
EOL

else
    cat >./shadowsocks-config.json <<EOL
{
"server":["::0", "0.0.0.0"],
"server_port":"${port}",
"password":"${password}",
"method":"${chosenMethod}",
"timeout":15,
"fast_open":true,
"reuse_port":true,
"nameserver":"${dnsServer}",
"mode":"tcp_and_udp"
}
EOL
fi

#TODO system optimizations for shadowsocks

echo
echo
echo "Your server has been set up!"
echo
echo "You can run your server with the command: ss-server -c ~/shadowsocks/shadowsocks-config.json"
echo "Your config was saved in ~/shadowsocks/shadowsocks-config.json and you can edit or view it later if you forget any infomation, for example your password."

exit 0